import {InputHandler} from "./js/input.js";
import {Particle} from "./js/particle.js";
import {Player} from "./js/player.js";
import {Angler1} from "./js/angler1.js";
import {Angler2} from "./js/angler2.js";
import {LuckyFish} from "./js/luckyfish.js";
import {HiveWhale} from "./js/hivewhale.js";
import {Drone} from "./js/drone.js";
import {Background} from "./js/background.js";
import {SmokeExplosion} from "./js/smokeexplosion.js";
import {FireExplosion} from "./js/fireexplosion.js";
import {UI} from "./js/UI.js";

// canvas setup
const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');

// native width and height does not change
const nHeight = 1080;
const nWidth = 1920;

// % of browser window to be taken up by the canvas
const windowPercentage = 1;

// the canvas' displayed width/height
// this is what changes when the window is resized
let cHeight = nHeight;
let cWidth = nWidth;

function resize() {
    cWidth = window.innerWidth;
    cHeight = window.innerHeight;
    // ratio of the native game size width to height
    const nativeRatio = nWidth / nHeight;
    const browserWindowRatio = cWidth / cHeight;

    // the browser window is too wide
    if (browserWindowRatio > nativeRatio) {
        // take up 90% of window height divisible by tile size
        // height must be changed first since width is based on it
        cHeight = Math.floor(cHeight * windowPercentage);
        // if (cHeight > maxWidth) cHeight = maxHeight;
        cWidth = Math.floor(cHeight * nativeRatio);
    } else {
        // browser window is too high
        // take up 90% of window width divisible by tile size
        // width must be changed first since height is based on it
        cWidth = Math.floor(cWidth * windowPercentage);
        // if (cWidth > maxWidth) cWidth = maxWidth;
        cHeight = Math.floor(cWidth / nativeRatio);
    }


    // set the canvas style width and height to the new width and height
    ctx.canvas.style.width = `${cWidth}px`;
    ctx.canvas.style.height = `${cHeight}px`;
    // ctx.imageSmoothingEnabled = false;
    //console.log(ctx.canvas.style.height, ctx.canvas.style.width)
}

window.addEventListener('load', function(){
    // canvas setup
    const canvas = document.getElementById('canvas1');
    const ctx = canvas.getContext('2d');

    //const fullScreenButton = document.getElementById('fullScreenButton')
    canvas.width = 1000;
    canvas.height = 500;
    const canvasPosition = canvas.getBoundingClientRect();
    const MUSIC_PATH = './assets/audio/music/';

    class Game {
        constructor(width, height, canvas) {
            this.width = width;
            this.height = height;
            this.background = new Background(this);
            this.player = new Player(this);
            this.input = new InputHandler(this, canvas, canvasPosition);
            this.ui = new UI(this);
            this.keys = [];
            this.enemies = [];
            this.particles = [];
            this.explosions = [];
            this.enemyTimer = 0;
            this.enemyInterval = 2000;
            this.ammo = 20;
            this.maxAmmo = 50;
            this.ammoTimer = 0;
            this.ammoInterval = 350;
            this.gameOver = false;
            this.score = 0;
            this.winningScore = 80;
            this.gameTime = 0;
            this.timeLimit = 30000;
            this.speed = 1;
            this.debug = false;
            this.backgroundMusic = new Audio();
            this.backgroundPlaylist = [
                "happy-times.mp3",
                "brainiac.mp3",
                "techno-fights.mp3",
            ];
            this.backgroundMusic.src = MUSIC_PATH + this.backgroundPlaylist[Math.floor(Math.random() * this.backgroundPlaylist.length)];
        }
        update(deltaTime){
            if(!this.gameOver){
                if(this.gameTime === 0){
                    //this.backgroundMusic.play(); // have to make game menu so that player will interact with the document
                }
            }

            if(!this.gameOver) this.gameTime += deltaTime;
            if(this.gameTime > this.timeLimit) this.gameOver = true;
            this.ui.update(deltaTime);
            this.background.update();
            this.background.layer4.update();
            this.player.update(deltaTime);
            if(this.ammoTimer > this.ammoInterval){
                if(this.ammo < this.maxAmmo) {
                    if(this.player.powerUp) this.ammo += 2;
                    else this.ammo++;
                }
                this.ammoTimer = 0;
            } else {
                this.ammoTimer += deltaTime;
            }
            this.particles.forEach(particle => particle.update());
            this.particles = this.particles.filter(particle => !particle.markedForDeletion);
            this.explosions.forEach(explosion => explosion.update(deltaTime));
            this.explosions = this.explosions.filter(explosion => !explosion.markedForDeletion);
            this.enemies.forEach(enemy => {
                enemy.update();
                if(this.checkCollision(this.player, enemy)){
                    this.addExplosion(enemy);
                    enemy.markedForDeletion = true;
                    for(let i = 0; i < enemy.score; i++){
                        this.particles.push(new Particle(this, enemy.x + enemy.width * 0.5, enemy.y + enemy.height * 0.5));
                    }
                    if(enemy.type === 'lucky') this.player.enterPowerUp();
                    else if(!this.gameOver) this.score--;
                }
                this.player.projectiles.forEach(projectile => {
                    if(this.checkCollision(projectile, enemy)) {
                        enemy.lives--;
                        projectile.markedForDeletion = true;
                        this.particles.push(new Particle(this, enemy.x + enemy.width * 0.5, enemy.y + enemy.height * 0.5));
                        if(enemy.lives <= 0){
                            for(let i = 0; i < enemy.score; i++){
                                this.particles.push(new Particle(this, enemy.x + enemy.width * 0.5, enemy.y + enemy.height * 0.5));
                            }
                            enemy.markedForDeletion = true;
                            this.addExplosion(enemy);
                            if(enemy.type === 'hive'){
                                for(let i = 0; i < 5; i++){
                                    this.enemies.push(new Drone(this, enemy.x + Math.random() * enemy.width, enemy.y + Math.random() * enemy.height * 0.5));
                                }
                            }
                            if(!this.gameOver) this.score += enemy.score;
                            //if(this.score > this.winningScore) this.gameOver = true;
                        }
                    }
                });
            });
            this.enemies = this.enemies.filter(enemy => !enemy.markedForDeletion);
            if(this.enemyTimer > this.enemyInterval && !this.gameOver) {
                this.addEnemy();
                this.enemyTimer = 0;
            } else {
                this.enemyTimer += deltaTime;
            }
        }
        draw(context){
            this.background.draw(context);
            this.ui.draw(context);
            this.player.draw(context);
            this.particles.forEach(particle => particle.draw(context));
            this.enemies.forEach(enemy => {
                enemy.draw(context);
            });
            this.explosions.forEach(explosion => {
                explosion.draw(context);
            });
            this.background.layer4.draw(context);
            if(this.debug){
                context.strokeRect(0, 0, this.width * 0.5, this.height);
                context.strokeRect(0, 0, this.width * 0.5, this.height * 0.5);
            }
        }
        addEnemy(){
            const randomize = Math.random();
            if(randomize < 0.3) this.enemies.push(new Angler1(this));
            else if(randomize < 0.6) this.enemies.push(new Angler2(this));
            else if(randomize < 0.7) this.enemies.push(new HiveWhale(this));
            else this.enemies.push(new LuckyFish(this));
        }
        addExplosion(enemy){
            const randomize = Math.random();
            if(randomize < 0.5) this.explosions.push(new SmokeExplosion(this, enemy.x + enemy.width * 0.5, enemy.y + enemy.height * 0.5));
            else this.explosions.push(new FireExplosion(this, enemy.x, enemy.y));
        }
        checkCollision(rect1, rect2){
            return (rect1.x < rect2.x + rect2.width &&
                rect1.x + rect1.width > rect2.x &&
                rect1.y < rect2.y + rect2.height &&
                rect1.height + rect1.y > rect2.y
            )
        }
        restart(){
            this.gameTime = 0;
            this.gameOver = false;
            this.score = 0;
            this.keys = [];
            this.enemies = [];
            this.particles = [];
            this.explosions = [];
        }
    }

    const game = new Game(canvas.width, canvas.height, canvas);
    let lastTime = 0;

    function animate(timeStamp){
        const deltaTime = timeStamp - lastTime;
        lastTime = timeStamp;
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        game.draw(ctx);
        game.update(deltaTime);
        requestAnimationFrame(animate);
    }

    resize();
    animate(1);
});

window.addEventListener('resize', () => {
    resize();
})

// sounds taken from https://mixkit.co/free-sound-effects/game/?page=2
// music taken from https://mixkit.co/free-stock-music/tag/video-game/