export class UI {
    constructor(game) {
        this.game = game;
        this.fontSize = 25;
        this.fontFamily = 'Bangers';
        this.color = 'white';
        this.winsound = new Audio;
        this.winsound.src = './assets/audio/completion-of-a-level.wav';
        this.timeAfterGameover = 0;
    }
    update(deltaTime){
        if(this.game.gameOver){
            if(this.timeAfterGameover === 0){
                this.winsound.play();
                this.timeAfterGameover += deltaTime;
            }
        } else {
            this.timeAfterGameover = 0;
        }

    }
    draw(context){
        context.save();
        context.fillStyle = this.color;
        context.shadowOffsetX = 2;
        context.shadowOffsetY = 2;
        context.shadowColor = 'black';
        context.font = this.fontSize + 'px ' + this.fontFamily;
        // score
        context.fillText('Score: ' + this.game.score, 20, 40)

        // timer
        const formattedTime = (this.game.gameTime * 0.001).toFixed(1);
        context.fillText('Timer: ' + formattedTime, 20, 100)

        // ammo
        context.fillText('Ammo: ' + this.game.ammo.toFixed(0), 20, 130)

        // game over messages
        if(this.game.gameOver){
            context.textAlign = 'center';
            let message1;
            let message2;
            if(this.game.score > this.game.winningScore){
                message1 = 'Most Wondrous!';
                message2 = 'Well done explorer!';
            } else {
                message1 = 'Blazes!';
                message2 = 'Get my repair kit and try again!';
            }
            context.font = '70px ' + this.fontFamily;
            context.fillText(message1, this.game.width * 0.5, this.game.height * 0.5 - 20);
            context.font = '25px ' + this.fontFamily;
            context.fillText(message2, this.game.width * 0.5, this.game.height * 0.5 + 20);
        }

        // ammo
        if(this.game.player.powerUp) context.fillStyle = '#ffffbd';
        for(let i = 0; i < this.game.ammo; i++){
            context.fillRect(20 + 5 * i, 50, 3, 20);
        }

        context.restore();
    }
}