export class Projectile {
    constructor(game, x, y) {
        this.game = game;
        this.x = x;
        this.y = y;
        this.width = 10;
        this.height = 3;
        this.speed = 3;
        this.markedForDeletion = false;
        this.image = document.getElementById('projectile');
        this.soundPowered = new Audio;
        this.soundPowered.src = './assets/audio/shot-powered.wav';

        this.soundUnpowered = new Audio;
        this.soundUnpowered.src = './assets/audio/shot-unpowered.wav';

        this.timeAfterShot = 0;
    }
    update(deltaTime){
        if(this.timeAfterShot === 0){
            if(!this.game.player.powerUp){
                this.soundUnpowered.play();
            } else if(this.game.player.powerUp) {
                this.soundPowered.play();
            }

            this.timeAfterShot += deltaTime;
        }
        this.x += this.speed;
        if(this.x > this.game.width * 0.8) this.markedForDeletion = true;
    }
    draw(context){
        context.drawImage(this.image, this.x, this.y)
    }
}