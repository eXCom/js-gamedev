export class InputHandler {
    constructor(game, canvas, canvasPosition) {
        this.game = game;
        this.canvas = canvas;
        this.canvasWidth = this.canvas.offsetWidth;
        this.canvasHeight = this.canvas.offsetHeight;
        this.touchY = '';
        this.touchTresholdY = this.canvasHeight * 0.1;
        window.addEventListener('keydown', e => {
            if(((e.key === 'ArrowUp') ||
                (e.key === 'ArrowDown')
            ) && this.game.keys.indexOf(e.key) === -1){
                this.game.keys.push(e.key);
            } else if(e.key === ' ' || e.key === 'keep firing') {
                if(!this.game.keys.includes('keep firing')) this.game.keys.push('keep firing');
            } else if (e.key === 'd'){
                this.game.debug = !this.game.debug;
            }
        });

        window.addEventListener('keyup', e => {
            if(this.game.keys.indexOf(e.key) > -1){
                this.game.keys.splice(this.game.keys.indexOf(e.key), 1);
            }

            if(e.key === 'keep firing' || e.key === ' '){
                this.game.keys.splice(this.game.keys.indexOf('keep firing'), 1);
            }
        });

        canvas.addEventListener('touchstart', e => {
            this.touchY = e.changedTouches[0].pageY;
            if(!(e.changedTouches[0].pageX - canvasPosition.left <= this.canvasWidth * 0.5)){
                if(!this.game.keys.includes('keep firing')) this.game.keys.push('keep firing');
            }
        });

        canvas.addEventListener('touchmove', e => {
            const swipeDistance = e.changedTouches[0].pageY - this.touchY;
            if(e.changedTouches[0].pageX - canvasPosition.left <= this.canvasWidth * 0.5){
                // left side of canvas
                if(this.game.keys.includes('keep firing')) this.game.keys.splice(this.game.keys.indexOf('keep firing'), 1);
                if(e.changedTouches[0].pageY - canvasPosition.top <= this.canvasHeight * 0.5 && this.game.keys.indexOf('swipe up') === -1)
                {
                    this.game.keys.push('swipe up');
                    if(this.game.keys.includes('swipe down')) this.game.keys.splice(this.game.keys.indexOf('swipe down'), 1);
                } else if (e.changedTouches[0].pageY - canvasPosition.top > this.canvasHeight * 0.5 && this.game.keys.indexOf('swipe down') === -1)
                {
                    this.game.keys.push('swipe down');
                    if(this.game.keys.includes('swipe up')) this.game.keys.splice(this.game.keys.indexOf('swipe up'), 1);
                    if(this.game.gameOver){
                        if(swipeDistance > this.touchTresholdY){
                            this.game.restart();
                        }
                    }
                }
            } else {
                // right side of canvas
                if(!this.game.keys.includes('keep firing')) this.game.keys.push('keep firing');
                if(this.game.keys.includes('swipe up')) this.game.keys.splice(this.game.keys.indexOf('swipe up'), 1);
                if(this.game.keys.includes('swipe down')) this.game.keys.splice(this.game.keys.indexOf('swipe down'), 1);
            }
        });

        canvas.addEventListener('touchend', e => {
            this.game.keys.splice(this.game.keys.indexOf('swipe up'), 1);
            this.game.keys.splice(this.game.keys.indexOf('swipe down'), 1);
            if(this.game.keys.includes('keep firing')) this.game.keys.splice(this.game.keys.indexOf('keep firing'), 1);
        });

        fullScreenButton.addEventListener('click', e => {
            this.toggleFullScreen();
        });
    }
    toggleFullScreen(){
        if(!document.fullscreenElement){
            this.canvas.requestFullscreen().catch(err => {
                alert('Error, cant enable full-screen mode: ' + err.message);
            }).then(e => {
                this.recalculateCanvasSizes(this.canvas);
            });
        } else {
            document.exitFullscreen();
            this.recalculateCanvasSizes(this.canvas);
        }
    }
    recalculateCanvasSizes(canvas){
        this.canvasWidth = canvas.offsetWidth;
        this.canvasHeight = canvas.offsetHeight;
    }
}